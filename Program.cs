﻿using System;
using System.Text;
// Обратите внимание, данную библиотеку нужно будет подключить
using System.ServiceModel;

namespace Server
{
    [ServiceContract]
    public interface IMyService
    {
        [OperationContract]
        double GetSum(double i, double j);

        [OperationContract]
        double GetMult(double i, double j);

        [OperationContract]
        double GetSubtr(double i, double j);

        [OperationContract]
        double GetDiv(double i, double j);
    }
    
    public class MyService : IMyService
    {
        public double GetSum(double i, double j)
        {
            return i + j;
        }

        public double GetMult(double i, double j)
        {
            return i * j;
        }

        public double GetSubtr(double i, double j)
        {
            return i - j;
        }

        public double GetDiv(double i, double j)
        {
            return i / j;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Инициализируем службу, указываем адрес, по которому она будет доступна
            var host = new ServiceHost(typeof(MyService), new Uri("http://localhost:8000/MyService"));
   
            // Добавляем конечную точку службы с заданным интерфейсом, привязкой (создаём новую) и адресом конечной точки
            host.AddServiceEndpoint(typeof(IMyService), new BasicHttpBinding(), "");
  
            // Запускаем службу
            host.Open();
            Console.WriteLine("Server started");
            Console.ReadLine();

            // Закрываем службу
            host.Close();
        }
    }
}